/*c program to print a number triangle*/
#include <stdio.h>

int pattern(int n);

int main(){
	int num;
	printf("Enter a number: ");
	scanf("%d",&num);
	pattern(num);
	return 0;
}


int pattern(int n){
	int i;
	if(n>0)
		pattern(n-1);
	for(i=n; i>0; i--){
		printf("%d",i);
	}
	printf("\n");
}

