/*c program to print the fibonacci function*/
#include <stdio.h>
int fibonacciSeq(int );

int main(){
	int num;
	printf("Enter a number: ");
	scanf("%d",&num);
	for(int n=0; n<num; n++){
	printf("%d\n",fibonacciSeq(n));
	}
	return 0;
 
}

int fibonacciSeq(int n){
	if (n==0) return 0;
	else if (n==1) return 1;
	else return fibonacciSeq(n-1)+fibonacciSeq(n-2);
}	
