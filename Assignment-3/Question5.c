/*C program to print multiplication table*/
#include <stdio.h>
int main(){
	int num, j, i, multiply;
	printf("Enter a number : ");
	scanf("%d",&num);

	for(i=1; i<=num; i++) {
		for(j=1; j<=10; j++) {
			multiply= i*j;
			printf("%d * %d = %d\n", i,j,multiply);
		}
		printf("\n");
	}

	return 0;
}
