/*C program to find factors of a given number*/
#include <stdio.h>
int main() {
	int num, i;
	printf("Enter a positive integer: ");
	scanf("%d", &num);
	printf("The factors of the number: ",num);

	i=1;
	while(i<=num)
	{
		if (num%i==0) printf("%d ",i);
		i ++;
	}
	printf("\n");


return 0;
}
