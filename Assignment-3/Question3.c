
/*C program to reverse a number*/
#include <stdio.h>
int main(){
	int num, reverse;
	printf("Enter a number: ");
	scanf("%d", &num);

	while (num != 0)
	{
		reverse = reverse * 10;
		reverse = reverse + num%10;
		num = num/10;

	}
	printf("Reverse of the entered number = %d\n", reverse);

return 0;
}
