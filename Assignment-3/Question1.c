/*C program calculate the sum of all positive intergers*/
#include <stdio.h>
int main(){
	int total, num;
	
	do {
		printf("Enter a number; ");
		scanf("%d", &num);
		if (num<=0) break;
		total += num;
	   
	}while(num>0);
	printf("Sum of the numbers %d\n",total);

return 0;
}
