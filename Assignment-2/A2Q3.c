
/*C program to recognize whether a given character is a vowel or consonant*/
#include <stdio.h>
int main(){
	char x;

	printf("Enter a character: ");
	scanf("%c", &x);

	switch(x){
		case 'a':
		case 'e':
		case 'i':
		case 'o':
		case 'u':
		case 'A':
		case 'E':
		case 'I':
		case 'O':
		case 'U':
			printf("Vowel\n");
			break;
		default : printf("Consonant\n");
	}
	return 0;
}

