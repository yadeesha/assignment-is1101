
/* C program to read two integers amd swap it*/
#include <stdio.h>
int main(){
	int num1,num2,x;
	printf("Enter two integers to be swapped\n");
	printf("First Number:");
	scanf("%d" , &num1);
	printf("Second Number:");
	scanf("%d", &num2);
	
	x=num1;
	num1=num2;
	num2=x;
	printf("After swapping, First Number = %d\n",num1);
	printf("After swapping, Second Number = %d\n",num2);

	return 0;
}
