
/*C program to compute the area of a disk*/
#include <stdio.h>
int main(){
	float radius, area, pi;
		pi = 3.14;

	printf("Enter the radius of disk: ");
	scanf("%f", &radius);

	area= pi * radius * radius;
	printf("The area of the disk is:%.3f\n", area);
return 0;
}
