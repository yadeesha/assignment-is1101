
/*C program to multiply two floating point numbers*/
#include <stdio.h>
int main() {
	float a, b, answer;
	printf("Enter two numbers to be multiplied:\n  ");
	scanf("%f  %f", &a,&b);
	answer = a * b;
	printf("Answer of the entered number is:%.3f", answer);
	return 0;
}
