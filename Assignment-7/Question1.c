/*C program to reverse a sentence*/
#include <stdio.h>
#include <string.h>
#define Size 100

int main(){
	char sen[Size];
	printf("Enter a sentence: ");
	fgets(sen,Size,stdin);

	int l=strlen(sen);
	
	for (int i=l; i>=0; i--) printf("%c", sen[i]);
	printf("\n");
	return 0;
}

