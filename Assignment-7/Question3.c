//C program to add and multiply two matrices
#include <stdio.h>

void readM(int r1, int r2,int c1,int c2,int M1[10][10],int M2[10][10]);
void add(int M1[10][10], int M2[10][10],int Add[10][10],int r1,int c1);
void multi(int M1[10][10],int M2[10][10],int mul[10][10],int r1,int c1, int c2);


int main(){
	int r1, c1, r2, c2, M1[10][10], M2[10][10], Add[10][10], mul[10][10];
	printf("Enter the number of rows and colums of the first matrix: ");
	scanf("%d %d", &r1, &c1);
	printf("Enter the number of rows and columns of the second matrix: ");
	scanf("%d %d", &r2, &c2);

	if(r1 == r2 && c1==c2){
		readM(r1, r2, c1, c2, M1, M2);

		printf("Addition of the two matrices: \n");
		add(M1,M2,Add,c1,r1);
		for(int i=0; i<r1; i++){
			for(int j=0; j<c1; j++) 
				printf("%d\t", Add[i][j]);
			printf("\n");
		}

		printf("Multiplication of the two matrices: \n");
		multi(M1, M2, mul, r1, c1, c2);
		for(int i=0; i<r1; i++){
			for(int j=0;j<c1; j++)
				printf("%d\t",mul[i][j]);
			printf("\n");
		}

	}

	else {
		printf("The number of rows and colums of the matrices does not match \nAddition and Multiplication is not possible\n");
	}
	return 0;
}

void readM(int r1, int r2, int c1, int c2, int M1[10][10], int M2[10][10]){
	printf("Enter values of first matrix\n");
	for(int i = 0; i<r1; i++){
		for(int j =0; j<c1; j++){
			printf("Enter element in row[%d] column[%d]: ",i+1, j+1);
			scanf("%d",&M1[i][j]);
		}
		printf("\n");
	}

	printf("Enter values for Second matrix\n");
	for(int i = 0; i<r2; i++){
		for(int j=0; j<c2; j++){
			printf("Enter element in row[%d] column[%d]: ", i+1, j+1);
			scanf("%d",&M2[i][j]);
		}
		printf("\n");
	}
}

void add( int M1[10][10], int M2[10][10], int Add[10][10], int r1, int c1){

       for(int i = 0; i<r1; i++){
       		for(int j = 0; j<c1; j++){
	Add[i][j]=0;
		}
       }

       for(int i=0; i<r1; i++){
	       for(int j=0; j<c1; j++){
		       Add[i][j]=M1[i][j] + M2[i][j];
	       }
       }
}
              
void multi( int M1[10][10], int M2[10][10], int mul[10][10], int r1, int c1, int c2){
	
	for(int i=0; i<r1; i++){
		for(int j=0; j<c2; j++){
			mul[i][j]=0;
		}
	}
	
	for(int i=0; i<r1; i++){
		for(int j=0; j<c2; j++){
			for(int k=0; k<c1; k++){
				mul[i][j] += M1[i][k]*M2[k][j];
			}
		}
	}
}
