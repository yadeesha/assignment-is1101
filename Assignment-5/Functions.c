#include <stdio.h>

int attendance(int p);
int cost(int p);
int rev(int p,int q);
int prof(int p, int q);

int attendance(int p){
	/*equation to find the number of attendees*/
	int q;
	return q=180 - (4*p);
}

int cost(int p){
	return 500 + (3* attendance(p));
}

int rev(int p,int q){
	return p*q;
}

int prof(int p,int q){
	/*calculating the profit */
	int profit=rev(p,q)-cost(p);
	return profit;
}

int main(){
	int i,x,y;
	for(i=0; i<50; i=i+5){
		x=attendance(i);
		y=prof(i,x);

		printf("When ticket price is %d attendance is %d and profit is %d\n",i,x,y);
	}
return 0;
}
