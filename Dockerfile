FROM gcc
MAINTAINER Yadeesha Weerasinghe
RUN apt-get update && apt-get install -y \
vim \
gdb \
git
RUN git config --global user.email "yadeeshaw@gmail.com"
RUN git config --global user.name "yadeesha"
